## Available Scripts

In the project directory, you can run:

### `yarn start`

If you do not have yarn installed you can run the project with:

### `mpn start`

but **before** you must run the comand:

### `npm install --save react-router-dom`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.