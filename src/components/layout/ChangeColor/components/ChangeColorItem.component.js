import React, { Component } from 'react';

export default class ChangeColorItem extends Component {
  
  render() {
    const { item: {name}, onClick, index, selected } = this.props
    const isSelected = index === selected;
    return (
      <div onClick={onClick} className={`ChangeColorItem ${isSelected ? 'ChangeColorItemSelected' : ''}`}>
        <div className={`ChangeColorItem__Text ${isSelected ? 'ChangeColorItem__TextSelected' : ''}`}>{name}</div>
      </div>
    );
  }
}