import React, { Component } from 'react';

export default class ChangeColorAction extends Component {
  static defaultProps = {
    imageStyle: ''
  }
  render() {
    const { name, image, onClick, imageStyle, style } = this.props
    return (
      <div onClick={onClick} className="ChangeColorActions" style={style}>
        <div className="ChangeColorActions__Text">{name}</div>
        <img className={`ChangeColorActions__Image ${imageStyle}`} src={image} alt="" />
      </div>
    );
  }
}