import React, { Component } from 'react';
import './ChangeColor.css';
import ChangeColorAction from './components/ChangeColorAction.component'
import cancelIcon from './assets/cancel-icon.png'
import tickIcon from './assets/tick.png'
import ChangeColorItem from './components/ChangeColorItem.component'
import axios from 'axios';

export default class ChangeColor extends Component {
  constructor(props) {
    super(props)
    this.state = { selected: null, data: [
      {name: 'Always exchange (green)', color: 'GREEN'},
      {name: 'Exchange only with other students (yellow)', color: 'YELLOW'},
      {name: 'Can\'t exchange (red)', color: 'RED'}
    ] }
  }

  componentDidMount() {
  }

  handleSelectedPress = async () => {
    const { selected, data } = this.state
    if(selected == null) return;
    const item = data[selected]
    const token = sessionStorage.getItem('token')
    await axios.post('/didacticActivity/update', {
       status: 'NO_CHANGES',
       id: this.props.item.id,
       color: item.color
     }, this.getConfig(token));

    this.props.onClose();
  }

  getConfig = token => {
    return ({
      headers: {
        'Authorization': `Bearer ${token}`
      }
    });
  }

  render() {
    const { item, onClose } = this.props
    const { selected, data } = this.state
    if(item == null) return null;
    return (
      <div className="ChangeColor__Container">
        <div className="ChangeColor__Header_Container">
          <div className="ChangeColor__Header_Text">
            {item.courseName}
          </div>
        </div>
        <div className="ChangeColor__Content">
          {data.map((item, key) => (
            <ChangeColorItem onClick={() => {this.setState({selected: key})}} key={key} index={key} selected={selected} item={item} />
          ))}
        </div>
        <div className="ChangeColor_Actions">
          <ChangeColorAction style={{backgroundColor: '#5F6769'}} name='ANULARE' image={cancelIcon} onClick={onClose} />
          <ChangeColorAction style={{backgroundColor: '#3C4245'}} name='SCHIMBA' image={tickIcon} onClick={this.handleSelectedPress} imageStyle="ChangeColorActions__Image_Tick"/>
        </div>
      </div>
    );
  }
}