import React,  { Component } from 'react';

export default class Box extends Component {
  render() {
    return (
      <div className="card">
        <div className="card-body">
          {this.props.children}
        </div>
      </div>
    )
  }
}