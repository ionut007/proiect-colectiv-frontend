import Collapsible from 'react-collapsible';
import React,  { Component } from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import "./Colapsable.css"
export default class CustomCollapsable extends Component {

  render() {
    const { items, active } = this.props
    console.log(items, active)
    return(
      <Collapsible transitionTime={400} trigger="Year 3">
        <FormGroup>
          {items.map(item => (
            <FormControlLabel
              key={item.id}
              control={<Checkbox
                name={"check111"}
                color={"grey"}
                onChange={ev => this.props.handleCheck(ev.target.checked, item)}
                defaultChecked={active.filter(act => act.id === item.id).length > 0}
                inputProps={{ 'aria-label': 'Checkbox A' }}
              />}
              label={item.courseName}
            />
          ))}
        </FormGroup>
      </Collapsible>

    );
  }

}