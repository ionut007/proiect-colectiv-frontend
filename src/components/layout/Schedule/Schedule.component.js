import React from 'react'
import './Schedule.css'
import ToggleButtonGroup from 'react-bootstrap/ToggleButtonGroup'
import ToggleButton from 'react-bootstrap/ToggleButton'

const ExchangeType = {
  EXCHANGEABLE: 'exchangeable',
  WITH_APPROVAL: 'with_approval',
  NOT_EXCHANGEABLE: 'not_exchangeable'
}

export default class Schedule extends React.Component {
  days = [ 'Hours/Day', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday']
  hours = [ '8:00', '10:00', '12:00', '14:00', '16:00', '18:00']

  constructor(props) {
    super(props)
    this.state = { week: 0 }
  }

  getCell = (hour, day, data) => {
    return data.filter(item => (item.hour === hour && item.day === day))[0];
  }

  renderCell = cell => {
    let cellStyle = '';
    if(cell == null) {
      return (<div className={`Schedule__Cell`}>{''}</div>)
    }

    const { canBeExchanged, group, room, tutor, color, courseName, id, specialization, type, activityType } = cell.item

    if(!canBeExchanged) {
      cellStyle = 'Schedule__Cell_Simple'
    } else if(color === 'GREEN') {
      cellStyle = 'Schedule__Cell_Exchangeable'
    } else if(color === 'RED') {
      cellStyle = 'Schedule__Cell_Not_Exchangeable'
    } else {
      cellStyle = 'Schedule__Cell_With_Approval'
    }

    return (
      <div key={id} onClick={() => this.props.onClick(cell.item)} className={`Schedule__Cell ${cellStyle}`}>
        <div className="Schedule_Cell_Title">{courseName}</div>
        {tutor != null && <div>Professor: {tutor.firstName + " " + tutor.lastName}</div>}
        <div>Sala: {room}</div>
        {group && <div>Grupa: {group}</div>}
        {group == null && specialization && <div>Specialization: {specialization}</div>}
        <div>Type: {type != null ? type.toLowerCase() : activityType.toLowerCase()}</div>
      </div>
    )
  }

  handleWeekChange = (week) => this.setState({week})
  render() {
    const { dataWeekEven, dataWeekOdd } = this.props
    const { week } = this.state
    const data = week === 0 ? dataWeekEven : dataWeekOdd
    return (
      <div>
        <ToggleButtonGroup name='week-group' type='radio' value={week} onChange={this.handleWeekChange}>
          <ToggleButton value={0}>Even week</ToggleButton>
          <ToggleButton value={1}>Odd week</ToggleButton>
        </ToggleButtonGroup>
        <div className="Schedule__Meta_Title">{week === 0 ? 'Even ' : 'Odd '} Week</div>
        <div className="Schedule__Container">
          {this.days.map((day, index) => (
            <div key={day} className={`${index === 0 ? 'Schedule__Column_Meta' : 'Schedule__Column'}`} style={{backgroundColor: index % 2 === 0 ? 'white' : '#EFEFEF'}} >
              <div className="Schedule__Header_Cell">{day}</div>
              {index === 0 && this.hours.map(hour => (
                <div key={hour} className="Schedule__Cell" style={{color: 'black', alignItems: 'center', padding: '0px'}}>{hour}</div>
              ))}
              {index !== 0 && this.hours.map(hour => this.getCell(hour, day, data)).map(this.renderCell)}
            </div>
          ))}
        </div>
      </div>
    )
  }
}
