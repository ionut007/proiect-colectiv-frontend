import Screen from './Screen.component'
import Box from './Box.component'
import Sidebar from './Sidebar/Sidebar.component'
import Schedule from './Schedule/Schedule.component'
import Modal from './Modal/Modal.component'
import ChangeClass from './ChangeClass/ChangeClass.component'
import Collapsable from './Collapsable/CustomCollapsable.component'
import ChangeColor from './ChangeColor/ChangeColor.component'

export {
  ChangeColor,
  Collapsable,
  Modal,
  Sidebar,
  Screen,
  Box,
  Schedule,
  ChangeClass
}
