import React, { Component } from 'react';
import './ChangeClass.css';
import ChangeClassAction from './components/ChangeClassAction.component'
import cancelIcon from './assets/cancel-icon.png'
import tickIcon from './assets/tick.png'
import ChangeClassItem from './components/ChangeClassItem.component'
import axios from 'axios';

export default class ChangeClass extends Component {
  constructor(props) {
    super(props)
    this.state = { selected: null, data: [] }
  }

  componentDidMount() {
    this.getData();
  }

  handleSelectedPress = async () => {
    const { selected, data } = this.state
    if(selected == null) return;
    const item = data[selected]
    console.log(selected)
    const token = sessionStorage.getItem('token')
    await axios.post('/requests/createRequest', {
       type: 'REQUEST',
       currentActivityId: this.props.item.id,
       requester: {
         id: sessionStorage.getItem('id')
       },
       activity: {
         id: item.id
       }
     }, this.getConfig(token));

    this.props.onClose();
  }

  getData = async () => {
    const token = sessionStorage.getItem('token')
    const { item } = this.props
    const { data } = await axios.post('/student/getActivitiesForChange', { courseName: item.courseName, id: item.id, type: item.type }, this.getConfig(token));
    this.setState({data})
  }

  getConfig = token => {
    return ({
      headers: {
        'Authorization': `Bearer ${token}`
      }
    });
  }

  render() {
    const { item, onClose } = this.props
    const { selected, data } = this.state
    if(item == null) return null;
    return (
      <div className="ChangeClass__Container">
        <div className="ChangeClass__Header_Container">
          <div className="ChangeClass__Header_Text">
            {item.courseName}
          </div>
        </div>
        <div className="ChangeClass__Content">
          {data.map((item, key) => (
            <ChangeClassItem onClick={() => {this.setState({selected: key})}} key={key} index={key} selected={selected} item={item} />
          ))}
        </div>
        <div className="ChangeClass_Actions">
          <ChangeClassAction style={{backgroundColor: '#5F6769'}} name='ANULARE' image={cancelIcon} onClick={onClose} />
          <ChangeClassAction style={{backgroundColor: '#3C4245'}} name='SCHIMBA' image={tickIcon} onClick={this.handleSelectedPress} imageStyle="ChangeClassActions__Image_Tick"/>
        </div>
      </div>
    );
  }
}