import React, { Component } from 'react';

export default class ChangeClassItem extends Component {

  getDay = (day) => {
    const days = {
      'MONDAY': 'Mon.',
      'TUESDAY': 'Tu.',
      'WEDNESDAY': 'Wed.',
      'THURSDAY': 'Th.',
      'FRIDAY': 'Fri.',
      'SATURDAY': 'Sat.'
    }

    return days[day]
  }

  getHours = (hours, duration) => {
    const hour = Number(hours.split(':')[0])
    const minutes = hours.split(':')[1]

    const startHour = hour < 9 ? `0${hour}` : `${hour}`
    const endHour = hour + duration < 9 ? `0${hour + duration}` : `${hour + duration}`
    return `${startHour}:${minutes} - ${endHour}:${minutes}`
  }

  getWeek = (week) => {
    if(week == null) return 'W1 + W2';
    if(week == 'EVEN') return 'W2';
    return 'W1';
  }
  render() {
    const { item: {weekType, group, tutor, dayOfTheWeek, time, semigroup, duration}, onClick, index, selected } = this.props
    const isSelected = index === selected;
    return (
      <div onClick={onClick} className={`ChangeClassItem ${isSelected ? 'ChangeClassItemSelected' : ''}`}>
        <div className={`ChangeClassItem__Text ${isSelected ? 'ChangeClassItem__TextSelected' : ''}`}>{this.getWeek(weekType)}</div>
        <div className={`ChangeClassItem__Text ${isSelected ? 'ChangeClassItem__TextSelected' : ''}`}>{`${group} ${semigroup != null ? `/${semigroup}` : ''}`}</div>
        <div className={`ChangeClassItem__Text ${isSelected ? 'ChangeClassItem__TextSelected' : ''}`}>{`${tutor.lastName} ${tutor.firstName}`}</div>
        <div className={`ChangeClassItem__Text ${isSelected ? 'ChangeClassItem__TextSelected' : ''}`}>{this.getDay(dayOfTheWeek)}</div>
        <div className={`ChangeClassItem__Text ${isSelected ? 'ChangeClassItem__TextSelected' : ''}`}>{this.getHours(time, duration)}</div>
      </div>
    );
  }
}