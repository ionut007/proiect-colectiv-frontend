import React, { Component } from 'react';

export default class ChangeClassAction extends Component {
  static defaultProps = {
    imageStyle: ''
  }
  render() {
    const { name, image, onClick, imageStyle, style } = this.props
    return (
      <div onClick={onClick} className="ChangeClassActions" style={style}>
        <div className="ChangeClassActions__Text">{name}</div>
        <img className={`ChangeClassActions__Image ${imageStyle}`} src={image} alt="" />
      </div>
    );
  }
}