import React,  { Component } from 'react';
import './Screen.css'

export default class Screen extends Component {
  static defaultProps = {
    className: ''
  }
  render() {
    const { className } = this.props
    return (
      <div className={`container-fluid Screen ${className}`}>
        {this.props.children}
      </div>
    )
  }
}