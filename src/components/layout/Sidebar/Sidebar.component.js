import React, { Component } from 'react'
import './Sidebar.css'
import SidebarComponent from 'react-sidebar'
import SidebarItems from './SidebarItems.component'

const mql = window.matchMedia(`(min-width: 800px)`);

export default class Sidebar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sidebarDocked: mql.matches,
      sidebarOpen: false
    };

    this.mediaQueryChanged = this.mediaQueryChanged.bind(this);
    this.onSetSidebarOpen = this.onSetSidebarOpen.bind(this);
  }

  componentWillMount() {
    mql.addListener(this.mediaQueryChanged);
  }

  componentWillUnmount() {
    mql.removeListener(this.mediaQueryChanged);
  }

  onSetSidebarOpen(open) {
    this.setState({ sidebarOpen: open });
  }

  mediaQueryChanged() {
    this.setState({ sidebarDocked: mql.matches, sidebarOpen: false });
  }

  render () {
    const { history } = this.props
    return (
      <SidebarComponent
        transitions={false}
        sidebarClassName="Sidebar__Background"
        sidebar={<SidebarItems history={history} />}
        open={this.state.sidebarOpen}
        docked={this.state.sidebarDocked}
        onSetOpen={this.onSetSidebarOpen}
      >
        {this.props.children}
      </SidebarComponent>
    )
  }
}