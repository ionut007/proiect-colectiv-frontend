import React from 'react'
import { GoDashboard } from 'react-icons/go'
import { IoMdLogOut } from 'react-icons/io'
import { MdKeyboardArrowRight } from 'react-icons/md'

export default class SidebarItems extends React.Component {
  categories = [
    { name: 'GENERAL', items: [
      {name: 'Dashboard', icon: () => <GoDashboard className="Sidebar_Item_Icon" />, onClick: () => {this.props.history.push('/dashboard')}},
      {name: 'Select activities', icon: () => <GoDashboard className="Sidebar_Item_Icon" />, onClick: () => {this.props.history.push('/select-activity')}},
      {name: 'All requests', icon: () => <GoDashboard className="Sidebar_Item_Icon" />, onClick: () => {this.props.history.push('/activity-change')}}

    ] },
    { name: 'OTHER', items: [{name: 'Logout', icon: () => <IoMdLogOut className="Sidebar_Item_Icon" />, onClick: () => { document.location = '/login'}  }]  }
  ]
  render() {
    let roles = JSON.parse(sessionStorage.getItem('roles'))
    if(roles == null) roles = []
    const isTeacher = roles.filter(item => item.type === 'TEACHER').length > 0
    const categories = this.categories.map(category => ({
      ...category,
      items: category.items.filter(item => (!isTeacher || item.name !== 'Select activities') && (!isTeacher ||  item.name !== 'All requests'))
    }))
    
    return (
      <div className="Sidebar__Items">
        <div className="Sidebar__Title">SCHED. <div className="Sidebar__Title_Meta">234/2</div></div>
        <div className="Sidebar__Items_List">
          <div className="Sidebar__Divider" />
          {categories.map(category => (
            <div>
              <div className="Sidebar__Category">
                <div className="Sidebar__Category_Title">{category.name}</div>
                {category.items.map(item => (
                  <div className="Sidebar_Meta_Container">
                    <div className="Sidebar__Item" onClick={item.onClick}>
                      {item.icon()}
                      <div className="Sidebar__Item_Title">{item.name}</div>
                    </div>
                    <MdKeyboardArrowRight className="Sidebar_Item_Icon_Meta Sidebar_Item_Icon" />
                  </div>
                ))}
              </div>
              <div className="Sidebar__Divider" />
            </div>
          ))}
        </div>
      </div>
    )
  }
}