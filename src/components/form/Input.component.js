import React,  { Component } from 'react';

export default class Input extends Component {
  static defaultProps = {
    className: ''
  }
  render() {
    const { className, placeholder, disableBasic, ...props } = this.props

    if(disableBasic) {
      return <input {...props} className={className} placeholder={placeholder} />  
    }
    return (
      <div className={`form-group Form__Input_Group ${className}`}>
        <label>{placeholder}</label>
        <input {...props} className="form-control" placeholder={placeholder} />  
      </div>
    )
  }
}