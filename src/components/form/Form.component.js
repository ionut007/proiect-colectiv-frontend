import React,  { Component } from 'react';

export default class Form extends Component {
  constructor(props) {
    super(props)
    this.state = { form: {} }
  }
  onSubmit = event => {
    const { onSubmit } = this.props
    onSubmit(event)
    event.preventDefault()
  }
  render() {
    return (
      <form onSubmit={this.onSubmit}>
        <div className="form-group">
          {this.props.children}
        </div>
      </form>
    )
  }
}