import React,  { Component } from 'react';
import { Collapsable, Sidebar, Modal, ChangeClass } from '@layout';
import { toast } from 'react-toastify'
import axios from 'axios';

export default class SelectActivity extends Component {
  constructor(props) {
    super(props)
    this.state = { item: null, availableCourses: [], enrolledCourses: [] }
  }

  componentDidMount() {
    this.getData()
  }

  getData = async () => {
    const token = sessionStorage.getItem('token')
    const id = sessionStorage.getItem('id')

    const { data : enrolledCourses } = await axios.post('/student/getActivities', { id }, this.getConfig(token));
    const { data : availableCourses } = await axios.get('/didacticActivity/getAllCourses', this.getConfig(token));
    this.setState({availableCourses, enrolledCourses})
  }

  componentDidMount() {
    this.getData();
  }

  getConfig = token => {
    return ({
      headers: {
        'Authorization': `Bearer ${token}`
      }
    });
  }

  handleCheck = async (checked, item) => {
    const token = sessionStorage.getItem('token')
    const id = sessionStorage.getItem('id')

    if(checked) {
      await axios.post('/student/createStudentActivity', {userId: id, activityId: item.id}, this.getConfig(token))
    } else {
      await axios.post('/student/deleteStudentActivity', {userId: id, activityId: item.id}, this.getConfig(token))
    }

    toast.success('Change was successful!')
  }

  render() {
    const { history } = this.props
    const { availableCourses, enrolledCourses } = this.state
    return (
      <Sidebar history={history}>
        <div className="Dashboard__Screen">
          <div className="Dashboard__Screen_Title">Select activities</div>
          <Collapsable items={availableCourses} active={enrolledCourses} handleCheck={this.handleCheck} />
        </div>
      </Sidebar>
    )
  }
}
