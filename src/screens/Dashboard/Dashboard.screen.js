import React,  { Component } from 'react';
import { Schedule, Sidebar, Modal, ChangeClass, ChangeColor } from '@layout';
import { Route } from "react-router-dom";
import axios from 'axios';

export default class Dashboard extends Component {
  constructor(props) {
    super(props)
    this.state = { item: null, dataOddWeek: [], dataEvenWeek: [] }
  }

  handleClick = item => {
    if(item.type === 'COURSE') return

    const { history, match: {url} } = this.props
    this.setState({item})
    history.push(`${url}/change`)
  }

  renderModal = () => {
    const { item } = this.state
    const { history } = this.props

    let roles = JSON.parse(sessionStorage.getItem('roles'))
    if(roles == null) roles = []

    const isTeacher = roles.filter(item => item.type === 'TEACHER').length > 0

    if(item == null) {
      history.push('/dashboard')
    }

    if(isTeacher) {
      return (
        <Modal>
          <div
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              height: '100%'
            }}
          >
            <ChangeColor onClose={() => {this.setState({item: null}); this.getData();}} item={item} />
          </div>
      </Modal>
      )
    }

    return (
      <Modal>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            height: '100%'
          }}
        >
          <ChangeClass onClose={() => {this.setState({item: null}); this.getData();}} item={item} />
        </div>
      </Modal>
    );
  }

  componentDidMount() {
    this.getData();
  }

  getData = async () => {
    const token = sessionStorage.getItem('token')
    const id = sessionStorage.getItem('id')
    let roles = JSON.parse(sessionStorage.getItem('roles'))
    if(roles == null) roles = []

    const isTeacher = roles.filter(item => item.type === 'TEACHER').length > 0
    if(isTeacher) {
      const { data } = await axios.post('/teacher/getActivities', { id }, this.getConfig(token));
      this.setState({dataEvenWeek: this.parseData(data, {weekType: 'EVEN'}), dataOddWeek: this.parseData(data, {weekType: 'ODD'})});
      return;
    }
    const { data } = await axios.post('/student/getActivities', { id }, this.getConfig(token));
    this.setState({dataEvenWeek: this.parseData(data, {weekType: 'EVEN'}), dataOddWeek: this.parseData(data, {weekType: 'ODD'})});
  }

  parseData = (data, extra = {}) => {
    const days = [ 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday' ]
    return data
    .filter(item => item.weekType == null || item.weekType === extra.weekType)
    .map(item => ({
      hour: item.time.substring(0, item.time.length - 3),
      day: days[new Date(item.date).getDay() - 1],
      id: item.id,
      item: {
        canBeExchanged: item.activityType !== 'COURSE' && item.type !== 'COURSE',
        ...item
      }
    }))
  }

  getConfig = token => {
    return ({
      headers: {
        'Authorization': `Bearer ${token}`
      }
    });
  }

  render() {
    const { dataEvenWeek, dataOddWeek } = this.state
    const { history } = this.props
    return (
      <Sidebar history={history}>
        <div className="Dashboard__Screen">
          <Route path={`/dashboard/change`} render={this.renderModal} />
          <div className="Dashboard__Screen_Title">Schedule</div>
          <Schedule dataWeekEven={dataEvenWeek} dataWeekOdd={dataOddWeek} onClick={this.handleClick} />
        </div>
      </Sidebar>
    )
  }
}
