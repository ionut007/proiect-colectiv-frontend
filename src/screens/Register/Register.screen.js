import React,  { Component } from 'react';
import {Row,Col,Form,FormLabel,Button} from 'react-bootstrap'
import "./Register.screen.css"
import { validate } from 'indicative/validator'
import { toast } from 'react-toastify';
import axios from 'axios'

export default class Register extends Component {
  notify = () => toast("Register successful!");
  error = () => toast("Error while registering!");
  constructor() {
    super();
    this.state = {
      lastName: '',
      firstName: '',
      email: '',
      password: '',
      password_confirmation: '',
      errors: Array(6).fill(null),
      grupa: '',
      semigrupa: ''
    };
  }

  register = async () => {
    const { lastName, firstName, email, password, grupa, semigrupa } = this.state
    console.log(this.state)
    console.log(grupa, semigrupa, grupa.match('23[0-9]') == null , (semigrupa !== '1' && semigrupa !== '2'))
    if(grupa.match('23[0-9]') == null || (semigrupa !== '1' && semigrupa !== '2')) {
      toast.error('Group or semigroup invalid!')
      return;
    }

    await axios.post('/users/createUser', {
      lastName,
      firstName,
      email,
      password,
      email,
      grupa,
      semigrupa,
      counter: 0,
      status: 1,
      username: email.split('@')[0],
      mobileNumber: '00000000'
    }).then(() => {
      this.props.history.push('/login')
      this.notify()
    })
  }
  handleSubmit = (event) => {

    this.setState({
      errors: Array(6).fill(null)
    });
    event.preventDefault();
    const rules = {
      password: 'required|min:4|confirmed'
    };
    const messages = {
      required: (field) => `${field} is required`,
      'password.confirmed': 'porblem we have at password'
    };
    validate(this.state, rules,messages)
      .then(() => {
        this.register();
      })
      .catch((errors) => {
        errors.forEach((error) => {
          const arr = this.state.errors.slice();
          arr[error.field] = "true";
          this.setState({
            errors: arr
          });
          this.error();
        })
      })
  };
  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    })
  };
  checkFor = (password) => {
    if( typeof this.state.errors[password] == "undefined")
      return false;
    else
      return true;
  };
  render() {
    const { history } = this.props
    var link = <div>I agree to the <a href={"https://en.wikipedia.org/wiki/Terms_of_service"}>Terms and conditions</a></div>
    return (

      <body className="Page">
        <div className="Center">
          <div className="leftDiv">
            <h6>INFO</h6>
            <p>
              Control your schedule!
            </p>
            <Button onClick={() => history.push('/login')} style={{backgroundColor:"white",color:"black", borderColor:"#719192"}} variant="primary" >
              Have an account
            </Button>
          </div>
          <div className="rightDiv">
            <h5 className="Register_Text">REGISTER FORM</h5>
            <div className="center-div">
              <Form onSubmit={this.handleSubmit}>
                <Row>
                  <Col>
                    <FormLabel>First Name</FormLabel>
                    <Form.Control name="firstName"  onChange={this.handleChange} />
                  </Col>
                  <Col>
                    <FormLabel>Last Name</FormLabel>
                    <Form.Control name='lastName'  onChange={this.handleChange} />
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <FormLabel>Grup</FormLabel>
                    <Form.Control name="grupa" onChange={this.handleChange}/>
                  </Col>
                  <Col>
                    <FormLabel>Semigroup</FormLabel>
                    <Form.Control name='semigrupa' onChange={this.handleChange}/>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <FormLabel>Your Email</FormLabel>
                    <Form.Control type="email" name="email" onChange={this.handleChange}/>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <FormLabel>Password</FormLabel>
                    <Form.Control type="password" name="password" onChange={this.handleChange} isInvalid={this.checkFor("password")} />
                    <Form.Control.Feedback type="invalid">No match</Form.Control.Feedback>
                  </Col>
                  <Col>
                    <FormLabel>Confirm Password</FormLabel>
                    <Form.Control type="password" name="password_confirmation" onChange={this.handleChange} isInvalid={this.checkFor("password")} />
                    <Form.Control.Feedback type="invalid">No match</Form.Control.Feedback>
                  </Col>
                </Row>
                <Row style={{padding:"16px"}}>
                  <Form.Check type="checkbox" label="I agree to the Terms and Conditions" />
                </Row>
                <Button style={{backgroundColor:"#719192", borderColor:"#719192"}} variant="primary" type="submit">Register</Button>
              </Form>
            </div>
          </div>

        </div>

      </body>
    )
  }
}
