import React from 'react';
import { Screen } from '@layout';
import { Input, Form } from '@form';
import iphone from './resources/IPHONE.png';
import playStore from './resources/google-play-badge.png';
import appStore from './resources/app-store-badge.png';
import './Login.css';
import axios from 'axios';
import { toast } from 'react-toastify'

class Login extends React.Component {
  constructor(props) {
    super(props)
    this.state = { email: '', password: '' }
  }

  handleSubmit = (ev) => {
    ev.preventDefault();
    const { email, password } = this.state
    const { history } = this.props
    axios.post('/authenticate',  {
      username: email,
      password: password
    }, {
      headers: {
        'Authenticate': 'Bearer test'
      }
    }).then(({data}) => {
      const { token } = data
      sessionStorage.setItem('id', data.id)
      sessionStorage.setItem('token', token)
      sessionStorage.setItem('username', data.username)
      sessionStorage.setItem('name', data.firstName + ' ' + data.lastName)
      sessionStorage.setItem('roles', JSON.stringify(data.roles))

      history.push('/dashboard')
    }).catch(() => toast.error('Username or password are incorrect!'))

    ev.preventDefault();
  }

  handleChangeEmail = ev => {
    this.setState({email: ev.target.value})
  }

  handleChangePassword = ev => {
    this.setState({password: ev.target.value})
  }

  render() {

    const { email, password } = this.state
    const { history } = this.props
    return (
      <Screen className="LoginScreen">
        <div className="flexbox">

          <div className="login-div">
            <h1>YOUR SCHEDULE</h1>
            <p>BETTER</p>
            <p>EASIER</p>
            <Form>
              <Input value={email} onChange={this.handleChangeEmail} disableBasic type='text' placeholder='Username' className='input-login' />
              <Input value={password} onChange={this.handleChangePassword} disableBasic type='password' placeholder='Password' className='input-login' />
              <input onClick={this.handleSubmit} type="submit" className={'input-submit'} value={'Log In'} />
              <input onClick={() => history.push('/register')} type="button" className={'input-submit'} value={'Register'} />
            </Form>
          </div>

          <div>
            <img src={iphone} className="logo" alt="iPhone" />
          </div>

          <div className="download-div">
            <p>
              You don't know where to go?
            </p>
            <p>
              Download our android or ios app and you'll find out
            </p>
            <img src={playStore} className="badge" alt="Android" />
            <img src={appStore} className="badge" alt="iOS" />

          </div>
        </div>
      </Screen>
    );
  }
}

export default Login;
