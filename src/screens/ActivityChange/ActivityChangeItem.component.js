import React, { Component } from 'react';

export default class ActivityChangeItem extends Component {
  
  render() {
    const { item: {requester: {firstName, lastName}, activity: {courseName, time, type, tutor}}, onClick, index, selected } = this.props
    const isSelected = index === selected;
    return (
      <div onClick={onClick} className={`ActivityChangeItem ${isSelected ? 'ActivityChangeItemSelected' : ''}`}>
        <div className={`ActivityChangeItem__Text ${isSelected ? 'ActivityChangeItem__TextSelected' : ''}`}>{`${firstName} ${lastName} wants "${courseName}" - ${type.toLowerCase()}, ${time} with ${tutor.firstName} ${tutor.lastName}`}</div>
      </div>
    );
  }
}