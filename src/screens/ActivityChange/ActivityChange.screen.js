import React,  { Component } from 'react';
import { Schedule, Sidebar, Modal, ChangeClass } from '@layout';
import {Button} from 'react-bootstrap'
import { Route } from "react-router-dom";
import axios from 'axios';
import ActivityChangeItem from './ActivityChangeItem.component'
import './ActivityChange.css'
import { toast } from 'react-toastify'

export default class ActivityChange extends Component {
  constructor(props) {
    super(props)
    this.state = { item: null, data: [], selected: null }
  }

  handleClick = item => {
    if(item.type === 'COURSE') return

    const { history, match: {url} } = this.props
    this.setState({item})
    history.push(`${url}change`)
  }

  renderModal = () => {
    const { item } = this.state
    const { history } = this.props

    if(item == null) {
      history.push('/dashboard')
    }

    return (
      <Modal>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            height: '100%'
          }}
        >
          <ChangeClass onClose={() => {this.setState({item: null})}} item={item} />
        </div>
      </Modal>
    );
  }

  componentDidMount() {
    this.getData();
  }

  getData = async () => {
    const token = sessionStorage.getItem('token')
    const id = sessionStorage.getItem('id')

    const { data } = await axios.get('/requests/getRequests?id=' + id, this.getConfig(token));
    this.setState({data})
  }


  getConfig = token => {
    return ({
      headers: {
        'Authorization': `Bearer ${token}`
      }
    });
  }

  handleAccept = async () => {
    const { history } = this.props
    const { selected, data } = this.state
    if(selected == null) return;
    const item = data[selected]
    const token = sessionStorage.getItem('token')

    await axios.post('/requests/createRequest', {
       type: 'ACCEPT',
       requester: {
         id: sessionStorage.getItem('id')
       },
       activity: {
         ...item.activity
       }
     }, this.getConfig(token));

     history.push('/dashboard')
     toast.info('Your request was accepted!')
  }

  render() {
    const { history } = this.props
    const { data, selected } = this.state
    console.log(data, selected)
    return (
      <Sidebar history={history}>
        <div className="Dashboard__Screen">
          <Route path={`/change`} render={this.renderModal} />
          <div className="Dashboard__Screen_Title">All requests for change</div>
          {data.length === 0 && <div>There are no request right now!</div>}
          <div className="ActivityChange__Content">
          {data.map((item, key) => (
            <ActivityChangeItem onClick={() => {this.setState({selected: key})}} key={key} index={key} selected={selected} item={item} />
          ))}
           <Button onClick={this.handleAccept} disabled={selected == null} style={{backgroundColor:"#719192", borderColor:"#719192"}} variant="primary" type="submit">Accept</Button>
        </div>
        </div>
      </Sidebar>
    )
  }
}
