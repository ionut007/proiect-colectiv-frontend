import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import { ToastContainer,toast} from 'react-toastify';
import Dashboard from '@screens/Dashboard/Dashboard.screen';
import Login from '@screens/Login/Login.screen'
import Register from '@screens/Register/Register.screen'
import ActivityChange from '@screens/ActivityChange/ActivityChange.screen'
import SelectActivity from '@screens/SelectActivity/SelectActivity.screen'
import './index.css';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import axios from 'axios';
import 'react-toastify/dist/ReactToastify.min.css';

axios.defaults.baseURL = 'http://localhost:3000/api'

axios.interceptors.response.use((response) => {
  return response;
}, function(error) {
  if(document.location.href.indexOf('login') === -1 && error.response.status === 401) {
    document.location = '/login'
  }
});

toast.configure();

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/register" render={props => <Register {...props} />} />
          <Route path="/login" render={props => <Login {...props} />} />
          <Route path="/dashboard" render={props => <Dashboard {...props} />} />
          <Route path="/activity-change" render={props => <ActivityChange {...props} />}/>
          <Route path="/select-activity" render={props => <SelectActivity {...props} />}/>
          <Redirect from='*' to='/404' />
        </Switch>
      </Router>
      <ToastContainer />
    </div>
  );
}

export default App;
